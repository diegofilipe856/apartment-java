public class Showcase {
    float discount;
    String uploadDate;
    String sellingDate;

    public void setDiscount (Float discount) {
        this.discount = discount;
    }
    public float getDisponible(){
        return this.discount;
    }

    public void setUploadDate (String uploadDate) {
        this.uploadDate = uploadDate;
    }
    public String getUploadDate(){
        return this.uploadDate;
    }

    public void setSellingDate (String sellingDate) {
        this.sellingDate = sellingDate;
    }
    public String getSellingDate(){
        return this.sellingDate;
    }
}
