public class Apartment implements Financeable {
    String numberOfPaviments;
    int numberOfRooms;
    float area;
    String location;
    String disponible;
    Boolean reformado;
    
    
    public Apartment(String numberOfPaviments,
    int numberOfRooms,
    float area,
    String location,
    String disponible,
    Boolean reformado) {
        this.setNumberOfPaviments(numberOfPaviments);
        this.setNumberOfRooms(numberOfRooms);
        this.setArea(area);
        this.setLocation(location);
        this.setDisponible(disponible);
        this.setReformado(reformado);

    }

    
    public void setDisponible(String disponible){
        this.disponible = disponible;
    }

    public String getDisponible(){
        return this.disponible;
    }

    public void setNumberOfRooms(int numberOfRooms){
        this.numberOfRooms = numberOfRooms;
    }

    public int getNumberOfRooms(){
        return this.numberOfRooms;
    }

    public void setArea(float area){
        this.area = area;
    }

    public float getArea(){
        return this.area;
    }

    public void setLocation(String location){
        this.location = location;
    }

    public String getLocation(){
        return this.location;
    }

    public void setReformado(Boolean reformado){
        this.reformado = reformado;
    }

    public Boolean getReformado(){
        return this.reformado;
    }
    public void setNumberOfPaviments(String numberOfPaviments){
        this.numberOfPaviments = numberOfPaviments;
    }

    public String getNumberOfPaviments(){
        return this.numberOfPaviments;
}
}